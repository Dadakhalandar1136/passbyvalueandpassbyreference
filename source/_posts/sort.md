---
title: Blog Post on Sort
date: 2023-03-06 18:33:23
tags:
---


![Sorting](https://www.shutterstock.com/image-photo/womans-hand-collects-even-row-600w-1141433123.jpg)

### JavaScript Array sort: Sorting Array Elements

Summary: in this, you will see how to use the JavaScript Array sort() method to sort arrays of strings, different case strings, integers, floating point numbers, objects, and multiple keys in an object.

#### Introduction to JavaScript Array sort() method

The sort() method allows you to sort elements of an array in place. Besides returning the sorted array, the sort() method changes the positions of the elements in the original array.

By default, the sort() method sorts the array elements in ascending order with the smallest value first and the largest value last.

The sort() method casts elements to strings and compares the strings to determine the order.

Consider the following example:

``` Js
    let numbers = [0, 1, 2, 3, 10, 20, 30];
    numbers.sort();
    console.log(numbers);
```
The output is:

``` Js
    [0, 1, 10, 2, 20, 3, 30]
```
In this example, the sort() method places 10 before 2 because the string "10" comes before "2" when doing a string comparison.

To fix this, you need to pass a compare function to the sort() method. The sort() method will use the compare function to determine the order of elements.

The following illustrates the syntax of the sort() method:
``` Js
    array.sort(compareFunction);
```
The sort() method accepts an optional argument which is a function that compares to elements of the array.

If you omit the compare function, the sort() method sorts the elements with the sort order based on the Unicode code point values of elements as mentioned earlier.

The compare function of the sort() method accepts two arguments and returns a value that determines the sort order. The following illustrates the syntax of the compare function:

``` Js
    function compare(start, end) {
        // ...
    }
```

The compare() function accepts two arguments a and b. The sort() method will sort elements based on the return value of the compare() function with the following rules:

1. If compare(start, end) is less than zero, the sort() method sorts "a" to a lower index than "b". In other words, "a" will come first.

2. If compare(start, end) is greater than zero, the sort() method sorts "b" to a lower index than "a", i.e., "b" will come first.

3. If compare(start, end) return zero, the sort() method considers 'a' equals 'b' and leaves their positions unchanged.

To fix the issue of sorting the number, you can use the following syntax:

``` Js
    let numbers = [0, 1, 2, 3, 10, 20, 30];
    numbers.sort(function(start, end) {
        if(start > end) return 1;
        if(start < end) return -1;
        return 0;
    });

    console.log(numbers);
```

Output:

``` Js
    [0, 1, 2, 3, 10, 20, 30]
```

![Example image to understand the sorting syntax](https://www.freecodecamp.org/news/content/images/2020/05/sort.png)


Or you can define the comparison function using the **arrow function** syntax:

``` Js
    let numbers = [0, 1, 2, 3, 4, 10, 20, 30];
    numbers.sort((start, end) => {
        if(start > end) return -1;
        if(start < end) return 1;
        return 0;
    });

    console.log(numbers);
```

And the following is the simplest since the elements of the array are numbers:

``` Js
    let numbers = [0, 1, 2, 3, 10, 20, 30];
    numbers.sort((start, end) => start - end);

    console.log(numbers);
```

![JavaScript sort() Method Image](https://res.cloudinary.com/practicaldev/image/fetch/s--vlQ0Qe1g--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/k61fnqcb61lsy6roojvk.jpg)

#### Sorting an Array of Strings

Suppose you have an array of strings named 'animals' as follows:

``` Js
    let animals = ['cat', 'dog', 'elephant', 'bee', 'ant'];
```

To sort the elements of the 'animals' array in ascending order alphabetically, you use the sort() method without passing the compare function as shown in the following example:

``` Js
    let animals = ['cat', 'dog', 'elephant', 'bee', 'ant'];
    animals.sort();

    console.log(animals);
```

Output:

``` Js
    ['ant', 'bee', 'cat', 'dog', 'elephant']
```

To sort the 'animals' array in descending order, you need to change the logic of the compare function and pass it to the sort() method as in the following example.

``` Js
    let animals = ['cat', 'dog', 'elephant', 'bee', 'ant'];

    animals.sort((start, end) => {
        if (start > end) {
            return -1;
        } else if (start < end){
            return 1;
        } else {
            return 0;
        }
    });

    console.log(animals);
```

Output: 

``` Js
    [ 'elephant', 'dog', 'cat', 'bee', 'ant' ]
```

#### Sort using Different Case Strings

Suppose you have an array that contains elements in both uppercase and lowercase as follows:

``` Js
    // sorting array with mixed cases
    let mixedCaseAnimals = ['Cat', 'dog', 'Elephant', 'bee', 'ant'];
```

To sort this array alphabetically, you need to use a custom compare function to convert all elements to the same case e.g., uppercase for comparison, and pass that function to the sort() method.

``` Js
    let mixedCaseAnimals = ['Cat', 'dog', 'Elephant', 'bee', 'ant'];

    mixedCaseAnimals.sort(function(start, end) {
        let x = start.toUpperCase(),
            y = end.toUpperCase();
        return x === y ? 0 : x > y ? 1 : -1;
    });
```

Output:

``` Js
    [ 'ant', 'bee', 'Cat', 'dog', 'Elephant' ]
```

#### Sorting an Array of Numbers (Integers)

Suppose you have an array of numbers named scores as in the following example.

``` Js
    let scores = [9, 80, 10, 20, 5, 70];
```

To sort an array of numbers numerically, you need to pass into a custom comparison function that compares two numbers.

The following example sorts the 'scores' array numerically in ascending order.

``` Js
    let scores = [9, 80, 10, 20, 5, 70];

    // sort numbers in ascending order
    scores.sort((start, end) => start - end);

    console.log(scores);
```

Output:

``` Js
    [ 5, 9, 10, 20, 70, 80]
```

To sort an array of numbers numerically in descending order, you just need to reverse the logic in the compare function as shown in the following example:

``` Js
    let scores = [9, 80, 10, 20, 5, 70];

    // descending order
    scores.sort((start, end) => end - start);
    
    console.log(scores);
```

Output:

``` Js
    [ 80, 70, 20, 10, 9, 5]
```

#### Sorting an Array of Floating Point Numbers

Suppose you have an array of floating point numbers named 'floatNumbers' as in the following example.

``` Js
    let floatNumbers = [82.11742562118049, 28.86823689842918, 49.61295450928224, 5.861613903793295];
```

To sort an array of floating point numbers, you need to pass into a custom comparison function that compares two numbers.

The following example sorts the 'floatNumbers' array numerically in ascending order.

``` Js
    let floatNumbers = [82.11742562118049, 28.86823689842918, 49.61295450928224, 5.861613903793295];

    // ascending order
    floatNumbers.sort((start, end) => { return start - end });

    console.log(floatNumbers);
```

Output:

``` Js
    [ 5.861613903793295, 28.86823689842918, 49.61295450928224, 82.11742562118049 ]
```

#### Sorting an Array of Objects by a specified property

The following is an array of employee objects, where each object contains three properties: name, salary, and hireDate.

``` Js
    let employees = [
        {name: 'Jhon', salary: 90000, hireDate: 'July 1, 2010'},
        {name: 'David', salary: 75000, hireDate: 'August 15, 2009'},
        {name: 'Ana', salary: 80000, hireDate: 'December 12, 2011'}
    ];
```

##### Sorting Objects by a Numeric property

The following example shows how to sort the employees by 'salary' in ascending order.

``` Js
    // sort by salary
    employees.sort(function (start, end) {
        return start.salary - end.salary;
    });

    console.table(employee);
```

Output:


![Table of Data](https://www.javascripttutorial.net/wp-content/uploads/2020/03/javascript-array-sort-by-salary.png)

This example is similar to the example of sorting an array of numbers in ascending order. The difference is that it compares the salary property of two objects instead.

##### Sorting Objects by a String property

To sort the employees array by name property case-insensitively, you pass the compare function that compares two strings case-insensitively as follows:

``` Js
    employees.sort(function (x, y) {
        let start = x.name.toUpperCase(),
            end = y.name.toUpperCase();
        return start == end ? 0 : start > end ? 1 : -1;
    });

    console.table(employees);
```

Output:

![Table of Data output](https://www.javascripttutorial.net/wp-content/uploads/2020/03/javascript-array-sort-by-object-property.png)

##### Sorting Objects by the Date property

Suppose, you wish to sort employees based on each employee's hire date.

The hire date data is stored in the 'hireDate' property of the employee object. However, it is just a string that represents a valid date, not the 'Date' object.

Therefore, to sort employees by hire date, you first have to create a valid 'Date' object from the date string, and then compare two dates, which is the same as comparing two numbers.

Here is the solution:

``` Js
    employees.sort(function (x, y) {
        let start = new Date(x.hireDate),
            end = new Date(y.hireDate);

        return start - end;
    });

    console.table(employees);
```

Output:

![Table of Data output](https://www.javascripttutorial.net/wp-content/uploads/2020/03/javascript-array-sort-by-date.png)

#### Sort an Array of Objects on Multiple Keys

Let's see how you can completely configure the sorting of an Array of objects.

Let's say we have the below data set for our entire example.

``` Js
    let objectVariabe = [
        { name: 'Mark',
            age: 30,
            rollNo: 'R01'
        },
        { name: 'Anne',
            age: 20,
            rollNo: 'R02'
        },
        { name: 'James',
            age: 40,
            rollNo: 'R03'
        },
        { name: 'Jerry',
            age: 30,
            rollNo: 'R04'
        },
        { name: 'Lucy',
            age: 30,
            rollNo: 'R05'
        },
        { name: 'Mark',
            age: 30,
            rollNo: 'R06'
        },
    ];
```

Looking at the raw data we have with a 'console.table(objectVariable)'

![Output Table](https://res.cloudinary.com/practicaldev/image/fetch/s--W9bbH45C--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/kdphmk8u69l8ozb4va2i.PNG)

##### Sorting on String

Using the above example, let us try and sort out the object.

``` Js
    ObjectVariable.sort(function(start, end) {
        return start.name.localCompare(end.name);
    });

    console.table(ObjectVariable);
```

![Output Table](https://res.cloudinary.com/practicaldev/image/fetch/s--RTHq4nHM--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/v12q6yl1427r57sausim.PNG)

This is similar to a SQL Statement
``` SQL
    SELECT * FROM OBJECTVARIABLE ORDER BY NAME;
```

##### Sorting on Number(the ES6 way)

With ES6, we can even write it as an inline function. Let's try and sort based on the number field age.

``` Js
    objectVariable.sort((start, end) => start.age - end.age);

    console.table(objectVariable);
```

![Output Table](https://res.cloudinary.com/practicaldev/image/fetch/s--uu8Mj5R8--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/jspephc1d6cmqlb3zmrd.PNG)

This is similar to a SQL Statement
``` SQL
    SELECT * FROM OBJECTVARIABLE ORDER BY AGE;
```

##### Multiple Key Sort

We can combine sorts using the || operator in the order of the sorting we need.
``` Js
    objectVariable.sort((start, end) => (start.age - end.age || start.name.localeCompare(end.name)));

    console.table(objectVariable);
```

![Output Table](https://res.cloudinary.com/practicaldev/image/fetch/s--G_0huCUq--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/u27vu0k022o8rx8ige9d.PNG)

This is similar to a SQL Statement
``` SQL
    SELECT * FROM OBJECTVARIABLE ORDER BY AGE, NAME;
```

##### Sort by name, and then Age
```Js
    objectVariable.sort((start, end) => (start.name.loacalCompare(end.name) || start.age - end.age));
```
##### Changing to Descending order

If we wanted Age and Name to be in descending order we just need to swap the above command with
``` Js
    objectVariable.sort((start, end) => (end.age - start.age || end.name.localCompare(start.name)));

    console.table(objectVariable);
```
![Output Table](https://res.cloudinary.com/practicaldev/image/fetch/s--voF_zJpJ--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/40fl85uthgaazevdl61l.PNG)


##### Extend to sort on all 3 keys/columns

Using the above logic, you can append how many ever sort columns you might need in the order you need them.
``` Js
    objectVariable.sort((start, end) => (start.name.localeCompare(end.name) || start.age - end.age || start.rollNo - end.rollNo));

    console.table(objectVariable);
```
![Output Table](https://res.cloudinary.com/practicaldev/image/fetch/s--uuYPVi9p--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/oplqkq3wyfq7nk6hvbbj.PNG)


#### **References**

* [Array.property.sort()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)
* [Video reference (sort)](https://www.youtube.com/watch?v=BbuLjEqFlw0)