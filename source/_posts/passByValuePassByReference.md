---
title: Pass by Value and Pass by Reference
date: 2023-03-01 18:38:11
tags:
---

![image](https://i.pinimg.com/originals/61/1a/cc/611acc8ce00d744e8fa1bcdd708ea717.jpg)

In this post, we will look into **Pass by Value** and **Pass by Reference** in JavaScript.

Before understanding the pass-by-value and pass-by-reference in JavaScript, first, let's understand what primitive and non-primitive data types are.

## **Primitive & Non-Primitive Data Types**

So, in JavaScript, the data types are divided into two broad categories:

* Primitive
* Non-primitive

Data types such as **string, number, null, undefined, symbols, and boolean** fall under the category of Primitive data types, while all **Objects, arrays, and functions** fall under the category of non-primitive or reference data types.

### <center>**Data types in JavaScript**</center>             

![imageOfDataTypes](https://f4n3x6c5.stackpathcdn.com/article/datatypes-in-javascript/Images/Presentation20.jpg)

The main difference between primitives & non-primitives is that **primitives are immutable** i.e. there is no way to change a primitive value once it gets created, whereas **non-primitives are mutable** i.e. the value of an object can be changed after it gets created.

### **Primitive data types**
```Js
    var string = 'Scaler Academy';
    string[7] = 'a';
    console.log(string) // Scaler Academy
```
**Note:**
```Js
    let myString = 'Scaler';
    console.log(myString); // Scaler

    myString = myString + 'Academy';
    console.log(myString); // Scaler Academy
```
In the example given above, first, we assigned Scaler to 'myString' then we console logged it, and we got Scaler in the console, then we appended the value "Academy" to the existing value of 'myString' and on the console logging it, we got Scaler Academy in the console. Now you may ask how it can happen as I already mentioned that primitive data types are immutable, and string is one of them.

To understand this, we need to break the process into steps:

1. First, the existing value of "myString" is fetched, i.e., "Scaler".
2. Then "Academy" is appended to that existing value.
3. Then the resultant value, i.e., "Scaler Academy" is allocated to a new block of memory.
4. Now, the "myString" object points to the newly created memory space (the space to which the resultant value is allocated in the third step)
5. Now, the previously created memory space (i.e., the space of myString = "String") is available for garbage collection.

Primitive data types are **compared by value.** If two values are the same, then they are strictly equal.
```Js
    let num1 = 30;
    let num2 = 30;
    num1 === num2; // true

    let string1 = 'Scaler Academy';
    let string2 = 'Scaler Academy';
    string1 == string2; // true
```
### **Non-primitive Data Types**
```Js
    let myArray = ['Scaler', 'Academy'];
    myArray[1] = 'Topics';
    console.log(myArray); // ['Scaler', 'Topics']
```
Objects and arrays are not compared by value. That means even if two objects and arrays have the same elements, respectively, they are not strictly equal.
```Js
    let obj1 = {
        'website': 'Scaler Topics',
        'topic': 'JavaScript'
    };

    let obj2 = {
        'website': 'Scaler Topics',
        'topic': 'JavaScript'
    };

    console.log(obj1 === obj2); // false

    let myArray1 = ['Scaler', 'Topics'];
    let myArray2 = ['Scaler', 'Topics'];
    
    console.log(myArray1 === myArray2); // false
```
Two objects are strictly equal only **if they refer to the same object.**
```Js
    let obj1 = {
        'website': 'Scaler Topics',
        'topic': 'JavaScript'
    }

    let obj2 = obj1;
    console.log(obj1 === obj2); // true
```
Non-primitive values are sometimes also referred to as **reference types** because instead of values, they are compared by reference.

```
Note: In JavaScript, primitive values are stored on the stack, while non-primitive values are stored in a heap. 
```
## **Pass by Value** : 
In Pass by Value, the Function is called by directly passing the value of the variable as the argument. Changing the argument inside the function doesn't affect the variable passed from outside the function.

**JavaScript always passes by value** so changing the value of the variable never changes the underlying primitive (String or Number).

Pass by value in JavaScript **requires more space** as the functions get a copy of the actual content therefore, a **new variable is created in the memory.**

In this concept, the equals operator plays a big role. when we create a variable, the equals operator notices whether you are assigning that variable a primitive or non-primitive value and then works accordingly.

```
Note: when we use the = operator, there is a function call (behind the scenes) where a pass-by-value (or reference) in JavaScript is done.
```

![imageLink](https://scaler.com/topics/images/pass-by-value.webp)

**Example code**
```Js
    let num1 = 70;
    let num2 = num1;

    console.log(num1); // 70
    console.log(num2); // 70

    num1 = 40;

    console.log(num1); // 40
    console.log(num2); // 70
```
Here, we have assigned num1 a value of 70. This creates a space in memory by the name num1 and addresses 2001 (assumption). When we create a variable num2 and assign it the value of num1, then the equals operator notices that we're dealing with primitive values thus it creates a **NEW SPACE in-memory** with address 2002 and assigns it a copy of num1's value, i.e. 70. Now we can see that both the variables have different spaces in the memory, and both have a value of 70.

Now, if we change the value of num1, then num2 will have no effect as it has its **own separate space in memory**, and now it has nothing to do with the value of num2 as they both have different spaces (address) in memory.

Let's understand this better with another example:
```Js
    function multiplication(temp) {
        temp = temp * 30;
        return temp;
    }

    let num = 30;
    let result = multiplication(num);
    console.log(num); // 30
    console.log(result); //1500
```
From the above code, we can see that the function multiplication takes an argument and changes its value.

Then we have declared a variable num, with a value of 30.

## **Pass by Reference**

In Pass by Reference, the Function is called by directly passing the reference/address of the variable as the argument. Changing the argument inside the function affects the variable passed from outside the function. In JavaScript objects and arrays follows pass-by-reference.

Unlike **Pass by Value** in JavaScript, **Pass by Reference** in JavaScript does not create a new space in the memory, instead, we pass the reference/address of the actual parameter, which means the function can access the original value of the variable. Thus, if we change the value of the variable inside the function, then the original value also gets changed.

It does not create a copy, instead, it works on the original variable, so all the changes made inside the function affect the original variable as well.

![imageLink](https://scaler.com/topics/images/pass-by-reference.webp)

Unlike pass-by-value in JavaScript, here, when the equal operator identifies that the variable obj1 is set equal to an object, it creates a new memory space and points obj1 to 3005 (address assumption). Now, when we create a new variable, obj2, and assign it to the value of obj1 the equals operator identifies that we are dealing with non-primitive data types; thus, it points to the same address that obj1 points to. Thus we can see that **no new memory space** is created instead, both the variables are pointing to the same address that obj1 was pointing to.
```Js
    let obj1 = {webside: 'Scaler Academy'}
    let obj2 = obj1;

    console.log(obj1) // {website: 'Scaler Academy'}
    console.log(obj2) // {website: 'Scaler Academy'}

    obj1.website = 'Scaler Topics'

    console.log(obj1) // {website: 'Scaler Topics'}
    console.log(obj2) // {website: 'Scaler Topics'}
```
In the above example, we have made a variable obj1 and set it equal to an object, then we have set the value of another variable obj2 equal to obj1.

As the equal operator identifies that we are dealing with non-primitive data types, so instead of creating a new memory space, it points obj2 to the same memory space that obj1 is pointed to. Thus when we change (mutable) the value of obj1, then the value of obj2 also gets changed since obj2 is also pointing to the same memory space as obj1 does.

### **Pass by Reference in Object (with Function)**
```Js
    let originalObj = {
        name: 'Scaler Academy',
        rating: 4.5,
        topic: 'JavaScript'
    };

    function demo(tempObj) {
        tempObj.rating = 5;
        console.log(tempObj.rating);
    }

    console.log(originalObj.rating);  // 4.5
    demo(originalObj);  // 5
    console.log(originalObj.rating);    // 5
```
### **Pass by Reference in an Array (with Function)**
```Js
    let original = ['Scaler', 'Academy', 'is', 'the'];

    function pushArray(tempArr) {
        tempArr.push('best');
        console.log(tempArr);
    }

    console.log(original);    // ['Scaler', 'Academy', 'is', 'the']
    pushArray(original);     // ['Scaler', 'Academy', 'is', 'the', 'best']
    console.log(original);   // ['Scaler', 'Academy', 'is', 'the', 'best']
```
### **When to Use Pass by Value?**

As in pass-by-value in JavaScript, a new copy of the variable is created, and any changes made in the new variable are independent of the original variable, so it is useful when we went to keep track of the initial variable and don't want to lose its value.

### **When to Use Pass by Reference?**

When we are passing arguments of large size, it is better to use pass-by-reference in JavaScript as no separate copy is made in the called function, so memory is not wasted, and hence the program is more efficient.

### **References**

* [Pass by Value and Pass by Reference](https://www.scaler.com/topics/javascript/pass-by-value-and-pass-by-reference/)
* [Visit](https://medium.com/nodesimplified/javascript-pass-by-value-and-pass-by-reference-in-javascript-fcf10305aa9c)